# Process Tensor Format
Define the `.processTensor` file format to store an MPS representing a process 
tensor as introduced in [Pollock2018]. 

This repository also ficillitates python3 functions that validate whether a 
given file is a valid `.processTensor` file or not.

## Version 1.0
*Created on 3rd of Feb 2020*

A valid `.processTensor` file version 1.0 is a serialised python3 dictionary
using pickel of the following form:
```python
dict[ "version"     : "1.0"
      "name"        : Optional[Text]
      "description" : Optional[Text]
      "note"        : Optional[Text]
      "t_offset"    : Optional[float]
      "dt"          : Optional[float]
      "parameters"  : Optional[dict]
      "tensors"     : list[ dict[ "name"      : Optional[Text],
                                  "from_time" : Optional[float],
                                  "to_time"   : Optional[float],
                                  "tensor"    : ndarray          ],
                            dict[ "name"      : Optional[Text],
                                  "from_time" : Optional[float],
                                  "to_time"   : Optional[float],
                                  "tensor"    : ndarray          ],
                                  ...
                          ]
    ]
```
where the `ndarray` is an `numpy.ndarray` with 3 or 4 axis. If there are only 
3 axis, a kronecker delta between 3 and 4 is assumed. The axis have the 
following meaning:

  * axis 1 connects to the previous tensor (the first has dimension 1)
  * axis 2 connects to the next tensor (the last has dimension 1)
  * axis 3 is the past physical leg
  * axis 4 is the future physical leg.

Furthermore, we assume the following python and numpy versions:

  * python3.6.9
  * numpy==1.14.0
  
### Examples
see the examples in `examples/good_files.py` to see how to generate a
valid `.processTensor` file.

```python
import numpy as np
import pickle

def create_short_verbose(filename) -> None:
    """
    Creates a processTensor file with only three timesteps and all optional metadata.
    """
    bond_dimA=7
    bond_dimB=9
    phys_dim=4
    
    np.random.seed(0)

    # 3-legged: 
    #  this means there is a kronecker delta between past 
    #  and future physical legs.
    t0=np.random.rand(1,bond_dimA,phys_dim) 
                                           
    # 4-legged: 
    #  this means there is no kronecker delta between past 
    #  and future physical legs.
    t1=np.random.rand(bond_dimA,bond_dimB,phys_dim,phys_dim)

    # 4-legged: 
    #  this means there is no kronecker delta between past 
    #  and future physical legs.
    t2=np.random.rand(bond_dimB,1,phys_dim,phys_dim)
    
    # this is the dictionary
    p_t_dict = {"version": "1.0",
                "name": "Spin Boson Model",
                "description": " Ohmic spectral denisty J(w) = 2 * alpha * wc * exp(w/wc). \n Coupled to single spin with $\sigma_z$ pauli operator. ",
                "note": " This process tensor was created with my greate new implementation. ",
                "t_offset": 30.0,
                "dt": 0.05, 
                "parameters": {"alpha":0.3,"wc":4.0},
                "tensors": [ {"name": "first tensor", 
                              "from_time": 0.0, 
                              "to_time": 0.05, 
                              "tensor": t0},
                             {"name": "tensor 001", 
                              "from_time": 0.05, 
                              "to_time": 0.1, 
                              "tensor": t1},           
                             {"name": "last tensor", 
                              "from_time": 0.1, 
                              "to_time": 0.15, 
                              "tensor": t2} #,...           
                           ]
               }
     
    # now, we save the dictionary as a pickle file
    with open(filename, 'wb') as output:
        pickle.dump(p_t_dict, output)
        
    return None
```
## LISENCE
MIT

## Bibliography

* **[Pollock2018]** Pollock, F.A., Rodríguez-Rosario, C., Frauenheim, T., Paternostro, M. and Modi, K., 2018. *Non-Markovian quantum processes: Complete framework and efficient characterization.* Physical Review A, 97(1), p.012127.


-------------------------------------------------------

**Author:** *Gerald E. Fux* (gefux)

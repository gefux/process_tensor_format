#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: gefux
"""

import numpy as np
import pickle

def create_small_bad_A(filename) -> None:
    """
    Creates a file that is NOT a propper processTensor file.
    """
    bond_dim=7
    phys_dim=4
    
    np.random.seed(0)

    # 3-legged: 
    #  this means there is a kronecker delta between past 
    #  and future physical legs.
    t0=np.random.rand(1,bond_dim,phys_dim) 
                                           
    # 4-legged: 
    #  this means there is no kronecker delta between past 
    #  and future physical legs.
    t1=np.random.rand(bond_dim,1,phys_dim,phys_dim)
    
    # this is the dictionary
    p_t_dict = {"version": "1.1",                   # !!!! this is why it's a bad file, should be "1.0"
                "tensors": [ {"tensor": t0},
                             {"tensor": t1}          
                           ]
               }
     
    # now, we save the dictionary as a pickle file
    with open(filename, 'wb') as output:
        pickle.dump(p_t_dict, output)
        
    return None
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: gefux
"""

import sys
sys.path.insert(0,'..')

import examples as ex

good_directory="./good_files"
ex.good_files.create_minimal(good_directory+"/minimal.processTensor")
ex.good_files.create_short_verbose(good_directory+"/short_verbose.processTensor")

bad_directory="./bad_files"
ex.bad_files.create_small_bad_A(bad_directory+"/small_bad_A.processTensor")


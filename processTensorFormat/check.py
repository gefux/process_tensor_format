#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: gefux
"""

import numpy as np
import pickle

def assert_tensors(tensors):
    """
    Assert a list of tensors is compatible with the processTensor form.
    """
    
    # check that all tensors are numpy ndarrays
    for tensor in tensors:
        assert(isinstance(tensor,np.ndarray))
    
    # check that all tensors have 3 or 4 legs
    for tensor in tensors:
        assert(len(tensor.shape)==3 or len(tensor.shape)==4)
    
    # check that first and last leg is dimension 1
    assert(tensors[0].shape[0]==1)
    assert(tensors[-1].shape[1]==1)
    
    # check that the bond dimension of successive tensors fits together
    for i in range(len(tensors)-1):
        assert(tensors[i].shape[1]==tensors[i+1].shape[0])
        
    
def assert_dict(p_t_dict):
    """
    Assert dictionary is of correct processTensor form.
    """

    # check that the version field exists and is "1.0"
    assert(p_t_dict["version"]=="1.0")

    # check that the tensors field exists and a list of dicts with "tensor in it    
    assert("tensors" in p_t_dict)
    assert(isinstance(p_t_dict["tensors"],list))
    for t in p_t_dict["tensors"]:
        assert("tensor" in t)
        
    # check that all tensors are of the right form
    tensors = [t["tensor"] for t in p_t_dict["tensors"]]
    assert_tensors(tensors)


def assert_file(filename):
    """
    Assert file is of correct processTensor form.
    """
    with open(filename, 'rb') as input:
        p_t_dict=pickle.load(input)
        assert_dict(p_t_dict)

    
def check_file(filename):
    """
    Check if file is of correct processTensor form.
    """
    try:
        assert_file(filename)
        return True
    except:
        return False
        
    
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: gefux
"""

import numpy as np
import pickle

def create_minimal(filename) -> None:
    """
    Creates a processTensor file with only two timesteps and no descriptions.
    """
    bond_dim=7
    phys_dim=4
    
    np.random.seed(0)

    # 3-legged: 
    #  this means there is a kronecker delta between past 
    #  and future physical legs.
    t0=np.random.rand(1,bond_dim,phys_dim) 
                                           
    # 4-legged: 
    #  this means there is no kronecker delta between past 
    #  and future physical legs.
    t1=np.random.rand(bond_dim,1,phys_dim,phys_dim)
    
    # this is the dictionary
    p_t_dict = {"version": "1.0",
                "tensors": [ {"tensor": t0},
                             {"tensor": t1}          
                           ]
               }
     
    # now, we save the dictionary as a pickle file
    with open(filename, 'wb') as output:
        pickle.dump(p_t_dict, output)
        
    return None
    
def create_short_verbose(filename) -> None:
    """
    Creates a processTensor file with only three timesteps and all optional metadata.
    """
    bond_dimA=7
    bond_dimB=9
    phys_dim=4
    
    np.random.seed(0)

    # 3-legged: 
    #  this means there is a kronecker delta between past 
    #  and future physical legs.
    t0=np.random.rand(1,bond_dimA,phys_dim) 
                                           
    # 4-legged: 
    #  this means there is no kronecker delta between past 
    #  and future physical legs.
    t1=np.random.rand(bond_dimA,bond_dimB,phys_dim,phys_dim)

    # 4-legged: 
    #  this means there is no kronecker delta between past 
    #  and future physical legs.
    t2=np.random.rand(bond_dimB,1,phys_dim,phys_dim)
    
    # this is the dictionary
    p_t_dict = {"version": "1.0",
                "name": "Spin Boson Model",
                "description": " Ohmic spectral denisty J(w) = 2 * alpha * wc * exp(w/wc). \n Coupled to single spin with $\sigma_z$ pauli operator. ",
                "note": " This process tensor was created with my greate new implementation. ",
                "t_offset": 30.0,
                "dt": 0.05, 
                "parameters": {"alpha":0.3,"wc":4.0},
                "tensors": [ {"name": "first tensor", 
                              "from_time": 0.0, 
                              "to_time": 0.05, 
                              "tensor": t0},
                             {"name": "tensor 001", 
                              "from_time": 0.05, 
                              "to_time": 0.1, 
                              "tensor": t1},           
                             {"name": "last tensor", 
                              "from_time": 0.1, 
                              "to_time": 0.15, 
                              "tensor": t2} #,...           
                           ]
               }
     
    # now, we save the dictionary as a pickle file
    with open(filename, 'wb') as output:
        pickle.dump(p_t_dict, output)
        
    return None
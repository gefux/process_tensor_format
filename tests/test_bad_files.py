#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: gefux
"""

import sys
sys.path.insert(0,'..')

import glob
import unittest
import processTensorFormat as ptf

class TestBadFiles(unittest.TestCase):
    
    def setUp(self):
        self.bad_files=glob.glob("tests/bad_files/*")
        assert(len(self.bad_files)>=1),"There are no files to check!"

    def test_all_bad_files(self):
        for filename in self.bad_files:
            self.assertFalse(ptf.check.check_file(filename))

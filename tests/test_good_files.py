#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: gefux
"""

import sys
sys.path.insert(0,'..')

import glob
import unittest
import processTensorFormat as ptf

class TestGoodFiles(unittest.TestCase):
    
    def setUp(self):
        self.good_files=glob.glob("tests/good_files/*")
        assert(len(self.good_files)>=1),"There are no files to check!"
        
    def test_all_good_files(self):
        for filename in self.good_files:
            self.assertTrue(ptf.check.check_file(filename))
